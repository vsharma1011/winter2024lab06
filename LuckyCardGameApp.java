import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] arg){
		//runApp();
		runGame();
		
	}
	public static void runGame(){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int numOfRound = 1;
		System.out.println("Thanks for Playing Lucky!");
		while(totalPoints < 5 && manager.getNumberOfCards() > 1 ){
			System.out.println("Round: " + numOfRound);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println("Current points: " + totalPoints + "\n*********************************************************");
			manager.dealCards();
			numOfRound++;
		}
		if(totalPoints < 5){
			System.out.println("Final Results: " + totalPoints + " Points" + " (You Lose)");
		}
		else{
			System.out.println("Final Result: " + totalPoints + " Points" + " (You Win)");
		}
	}
	// //Num being between 1 to 52 not working
	// public static int isNumber(){
		// Scanner reader = new Scanner(System.in);
		// boolean validAns = false;
		// int userAnswer = reader.nextInt();
		// while(!validAns){
			// if(userAnswer >= 0 && userAnswer <= 52){
				// validAns = true;
			// }
			// else{
				// System.out.println("ERROR, Enter a number between 1 to 52");
				// userAnswer = reader.nextInt();
			// }
		// }
		// return userAnswer;
	// }
	// public static void reduceCards(int userInput, Deck deck){
		// for(int i = 0; i < userInput; i++){
			// deck.drawTopCard();
		// }
	// }
	// public static void runApp(){
		// //Intializes the deck object
		// Deck deck = new Deck();
		// //Uses the shuffle method to randomize the placement of where the cards appear on the list
		// deck.shuffle();
		// //Call to methods to ask user to remove # of cards
		// System.out.println("Hello!, Enter a NUMBER of cards you want to remove between 1 to 52");
		// int userAns= isNumber();
		// //Print deck length (no cards removed)
		// System.out.println("Deck has " + deck.length() + " (no change)");
		// //Print # of cards - the amt removed by user
		// reduceCards(userAns, deck);
		// System.out.println("Deck has " + deck.length());
		// //Print all cards except the ones that were removed
		// deck.shuffle();
		// System.out.println(deck);
	// }
}